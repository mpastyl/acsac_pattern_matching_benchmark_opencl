cmake_minimum_required(VERSION 3.1)

project(DFC-benchmark-libraries)
set(CMAKE_CXX_STANDARD 11)

set( CMAKE_CXX_DEFAULT_FLAGS "${CMAKE_CXX_FLAGS}")
set( CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS}" )
set( CMAKE_C_FLAGS  "${CMAKE_C_FLAGS}" )

add_subdirectory(libraries)
add_subdirectory(benchmarks)
