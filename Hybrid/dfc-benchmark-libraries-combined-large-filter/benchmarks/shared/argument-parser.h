#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

bool is_valid_arguments(int argc) {
  return argc == 3 || argc == 4;
}

void print_usage(char **argv) {
  printf("Usage: %s PATTERN-FILE DATA-FILE [ENERGY-FILE]\n", argv[0]);
}

const char* get_data_file(char *const * argv) {
  return argv[2];
}

const char* get_pattern_file(char *const *argv) {
  return argv[1];
}

const char* get_energy_file(int argc, char *const *argv) {
  if (argc == 3) {
    return NULL;
  }
  return argv[3];
}

void validate_arguments(int argc, char** argv) {
  if (!is_valid_arguments(argc)) {
    print_usage(argv);
    exit(0);    
  }
}
