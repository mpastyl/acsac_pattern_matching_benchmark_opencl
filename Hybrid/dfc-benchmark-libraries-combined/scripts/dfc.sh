#! /bin/bash

make -C build -j8 benchmark-dfc-opencl > /dev/null

if [ $? -eq 0 ]
then
  cd ../dfc-opencl-combined/build
  ../../dfc-benchmark-libraries-combined/build/benchmarks/dfc-opencl/benchmark-dfc-opencl "$@"
  cd ../../dfc-benchmark-libraries-combined
fi
