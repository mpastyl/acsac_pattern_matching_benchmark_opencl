# Benchmarking code for DFC IoT thesis

## Requirements
- CMake (3.1 or greater)
- OpenCL (1.2 or greater)
- C compiler
- C++ compiler (C++11 or greater)
- git


## Scripts
### Benchmarking scripts
**NOTE**
Before running any benchmarking script, make sure to create a build folder and generate make files
```sh
mkdir build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Release
```


Any script used for benchmarking should be executed **in the root of this repository** (henceforth called the root folder)
These are:
- `dfc.sh`
  - Assumes that there exists a directory `dfc-opencl` one level up from the root directory
  - If given a third argument, it will start the measurements of an already running `energy-monitor`
- `ac-snort.sh`
- `ffbf.sh`
  - Assumes that there exists a directory `pfac` one level up from the root directory
  - Not sure if it still works as I didn't use it in the end
- `pfac.sh`
  - Assumes that there exists a directory `pfac` one level up from the root directory
  - Not sure if it still works as I didn't use it in the end
- `ac-old.sh`
  - I do no longer guarantee that this one works as I didn't use it

### Other scripts
- `dfc-generator.py`: Will update the `CMakeLists.txt` of DFC
  - Should be executed from the root of the repository
  - Assumes that there exists a directory `dfc-opencl` one level up from the root directory
  - Used for running different configurations of DFC to find the best config
- `all.py`: Run all pattern data sets over all traffic data sets
  - Should be executed from the root of the repository
  - Assumes that all pattern data sets are in the same directory
  - Assumes that all traffic data sets are in the same directory
- `all-to-csv.py`: Will extract the search time (through the given regex) and energy from the output from `all.py`
- `all-visualization.py`
  - Requires `pyplot`
- `visualize-dfc-phases.py`: Energy graphs
  - Requires `pyplot`
- `energy-division.sh`: Simple calculation of energy percentage of algorithm
