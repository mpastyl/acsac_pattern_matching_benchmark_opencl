#include <signal.h>
#include <stdio.h>
#include <sys/wait.h>
#include <unistd.h>

#include "argument-parser.h"
#include "dfc.h"
#include "parser.h"
#include "timer.h"
#include "pfac.h"
#include "pfac.c"

#define SLEEP_DURATION (0)

int num_patterns;
char patterns[MAX_PATS_NUM][MAX_PAT_LEN + 1];
pid_t setupMeasChild(const char* energy_file);
void startEnergyMeas(pid_t child);

int readDataFile(int to_read_count, int max_pattern_length, char *buffer);
DFC_PATTERN_INIT *addPatterns(const char *pattern_file);
DFC_STRUCTURE *compilePatterns(DFC_PATTERN_INIT *init_struct);

int benchmarkSearch();
void printResult(DFC_FIXED_PATTERN *pattern);
void printTimers();

FILE *data_file;
size_t data_file_length;

long totalMatchCount;

void add_pattern(void *unused, unsigned char* pattern, int length, int case_insensitive, int pattern_id) {
  (void)(unused);
  (void)(case_insensitive);
  (void)(pattern_id);

  if (num_patterns < MAX_PATS_NUM) {
    memcpy(patterns[num_patterns], (char*)pattern, length);
    patterns[num_patterns][length] = '\a'; //I had to use a character not found in the patterns
                                           //to signify the end of the pattern 
    ++num_patterns;
  }
}

int main(int argc, char **argv) {
  validate_arguments(argc, argv);

	//******** PFAC init ********
	//ocl_init();
  parse_pattern_file(get_pattern_file(argv), NULL, (AddPattern)add_pattern);

  // build automaton, convert automaton

  PFAC_SINT_t *array_automaton;
  PFAC_automaton_t *pfac_automaton =
      pfac_build_automaton(patterns, num_patterns);
  printf("Automaton all nodes num %d\n",pfac_automaton->all_nodes_num);
  if (pfac_automaton){
    array_automaton = pfac_convert_automaton(pfac_automaton);
  }
  else
    return -1;
	//**************************


  pid_t child;
  bool should_meas_energy = get_energy_file(argc, argv) != NULL;
  if (should_meas_energy) {
      child = setupMeasChild(get_energy_file(argc, argv));
  }

  data_file = fopen(get_data_file(argv), "rb");
  if (data_file == NULL) {
    fprintf(stderr, "Data file not found\n");
    exit(1);
  }
  fseek(data_file, 0, SEEK_END);
  data_file_length = ftell(data_file);
  fseek(data_file, 0, SEEK_SET);

  DFC_SetupEnvironment();

  sleep(SLEEP_DURATION);

  DFC_PATTERN_INIT *init_struct = addPatterns(get_pattern_file(argv));

  sleep(SLEEP_DURATION);

  compilePatterns(init_struct);

  sleep(SLEEP_DURATION);

  if (should_meas_energy) {
    startEnergyMeas(child);
    sleep(2);
  }

  int matchCount = benchmarkSearch(array_automaton, pfac_automaton->root->id );
  printf("\n* Total match count return: %d\n", matchCount);
  printf("\n* Total match count onMatch: %ld\n", totalMatchCount);

  if (should_meas_energy) {
    if (kill(child, SIGINT) == 1) {
      perror("kill");
    }


    if (waitpid(child, NULL, 0) == -1) {
      perror("wait");
    }

    sleep(2);
  }

  sleep(SLEEP_DURATION);

  DFC_FreeStructure();
  DFC_FreePatternsInit(init_struct);

  DFC_ReleaseEnvironment();

  fclose(data_file);

  printTimers();
}

pid_t setupMeasChild(const char* energy_file) {
  pid_t child = fork();

  if (child == 0) {
    FILE *energy = fopen(energy_file, "w");
    if (energy == NULL) {
      perror("Open energy file");
    }


    if (dup2(fileno(energy), STDOUT_FILENO) == -1) {
      perror("Duplicating stdout");
    }


    if (fclose(energy) != 0) {
      perror("Close energy");
    }

    //execl("/home/odroid/Documents/EnergyMonitor-master/build/energy-monitor", "energy-monitor",
    //NULL);
    perror("Exec energy");

    exit(1);
  }

  return child;
}

void startEnergyMeas(pid_t child) {
  if (kill(child, SIGUSR1) != 0) {
    perror("Start energy");
  }
}

int readDataFile(int to_read_count, int max_pattern_length, char *buffer) {
  startTimer(TIMER_READ_DATA);

  size_t offset = ftell(data_file);
  if (offset && offset != data_file_length) {
    fseek(data_file, offset - (max_pattern_length - 1), SEEK_SET);
  }

  int actually_read_count =
      fread(buffer, sizeof(char), to_read_count, data_file);

  stopTimer(TIMER_READ_DATA);

  return actually_read_count;
}

DFC_PATTERN_INIT *addPatterns(const char *pattern_file) {
  DFC_PATTERN_INIT *init_struct = DFC_PATTERN_INIT_New();

  startTimer(TIMER_ADD_PATTERNS);
  parse_pattern_file(pattern_file, init_struct, (AddPattern)DFC_AddPattern);
  stopTimer(TIMER_ADD_PATTERNS);

  return init_struct;
}

DFC_STRUCTURE *compilePatterns(DFC_PATTERN_INIT *init_struct) {
  startTimer(TIMER_COMPILE_DFC);
  DFC_STRUCTURE *dfc = DFC_Compile(init_struct);
  stopTimer(TIMER_COMPILE_DFC);

  return dfc;
}

int benchmarkSearch( PFAC_SINT_t *array_automaton, int root_id) {
  startTimer(TIMER_SEARCH);
  int matchCount = DFC_Search(readDataFile, printResult, array_automaton, root_id);
  stopTimer(TIMER_SEARCH);

  return matchCount;
}

void printResult(DFC_FIXED_PATTERN *pattern) {
  totalMatchCount += pattern->external_id_count;
}

void printTimers() {
  printf("\n");
  printf("Environment setup: %f\n", readTimerMs(TIMER_ENVIRONMENT_SETUP));
  printf("Environment teardown: %f\n", readTimerMs(TIMER_ENVIRONMENT_TEARDOWN));
  printf("\n");

  printf("Adding patterns: %f\n", readTimerMs(TIMER_ADD_PATTERNS));
  printf("Reading data from file: %f\n", readTimerMs(TIMER_READ_DATA));
  printf("\n");

  printf("DFC Preprocessing: %f\n", readTimerMs(TIMER_COMPILE_DFC));
  printf("\n");

  printf("OpenCL write to device: %f\n", readTimerMs(TIMER_WRITE_TO_DEVICE));
  printf("OpenCL read from device: %f\n", readTimerMs(TIMER_READ_FROM_DEVICE));
  printf("\n");

  printf("OpenCL executing kernel: %f\n", readTimerMs(TIMER_EXECUTE_KERNEL));
  printf("CPU process matches (GPU version): %f\n",
         readTimerMs(TIMER_PROCESS_MATCHES));
  printf("CPU process matches (Heterogeneous version): %f\n",
         readTimerMs(TIMER_EXECUTE_HETEROGENEOUS));
  printf("Effective time (kernel + mem transfers) %f\n", readTimerMs(TIMER_EXECUTE_KERNEL)+readTimerMs(TIMER_WRITE_TO_DEVICE) + readTimerMs(TIMER_READ_FROM_DEVICE));
  printf("\n");

  printf("Total search time: %f\n", readTimerMs(TIMER_SEARCH));
}
