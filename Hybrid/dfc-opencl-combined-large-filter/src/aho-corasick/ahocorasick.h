/*
 * ahocorasick.h: the main ahocorasick header file.
 * This file is part of multifast.
 *
    Copyright 2010-2012 Kamiar Kanani <kamiar.kanani@gmail.com>

    multifast is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    multifast is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with multifast.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _automaton_H_
#define _automaton_H_

#include "node.h"
#define REALLOC_CHUNK_ALLNODES 200

typedef struct {
  /* The root of the Aho-Corasick trie */
  AC_NODE_t *root;

  /* maintain all nodes pointers. it will be used to access or release
   * all nodes. */
  AC_NODE_t **all_nodes;

  unsigned int all_nodes_num; /* Number of all nodes in the automaton */
  unsigned int all_nodes_max; /* Current max allocated memory for *all_nodes */

  AC_MATCH_t match;               /* Any match is reported with this */
  MATCH_CALBACK_f match_callback; /* Match call-back function */

  /* this flag indicates that if automaton is finalized by
   * ac_automaton_finalize() or not. 1 means finalized and 0
   * means not finalized (is open). after finalizing automaton you can not
   * add pattern to automaton anymore. */
  unsigned short automaton_open;

  /* It is possible to feed a large input to the automaton chunk by chunk to
   * be searched using ac_automaton_search(). in fact by default automaton
   * thinks that all chunks are related unless you do ac_automaton_reset().
   * followings are variables that keep track of searching state. */
  AC_NODE_t *current_node;     /* Pointer to current node while searching */
  unsigned long base_position; /* Represents the position of current chunk
  related to whole input text */

  /* Statistic Variables */
  unsigned long total_patterns; /* Total patterns in the automaton */

} AC_automaton_t;

AC_automaton_t *ac_automaton_init(MATCH_CALBACK_f mc);
AC_ERROR_t ac_automaton_add(AC_automaton_t *thiz, AC_PATTERN_t *str);
void ac_automaton_finalize(AC_automaton_t *thiz);
int ac_automaton_search(AC_automaton_t *thiz, AC_TEXT_t *str, void *param);
int pac_automaton_search(int *state_machine, int *lookup_table, int sm_length,
                         int lt_length, AC_TEXT_t *txt, void *param);
void ac_automaton_reset(AC_automaton_t *thiz);
void ac_automaton_release(AC_automaton_t *thiz);
void ac_automaton_display(AC_automaton_t *thiz, char repcast);
void start_measure_time(int);
void stop_measure_time(int);

#endif
