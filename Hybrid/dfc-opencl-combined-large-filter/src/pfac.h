/*
 * pfac.h
 *
 *  Created on: Feb 12, 2014
 *      Author: arishop
 */

#ifndef PFAC_H_
#define PFAC_H_

#include "aho-corasick/ahocorasick.h"
#include <CL/cl.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>

/*****************************************/
//#define ENG_MEASURE
#define SLEEP_TIME 1
//#define USE_LOCAL
#define USE_MAP

#ifndef USE_MAP
//	#define USE_MAP_OPT
#endif

//#define WORK_GROUP_SIZE 256
#define FACTOR 12

//#define FERMI
#ifdef FERMI
#define MAX_PATS_NUM 1000
#define MAX_IN_LEN 50000000
#else
#define MAX_PATS_NUM 3000
#define MAX_IN_LEN 200000000
#endif

#define MAX_OUTGOING 500
#define MAX_NODES 64000 // more than this, results in short int overflow
#define MAX_ALPHA 256
#define MAX_PAT_LEN 128

/*****************************************/

#define PLATFORM 0
#define DEVICE 1
#define CONTEXT 2
#define CMDQ 3
#define PGM 4
#define KERNEL 5
#define KERNEL_EXEC 6
#define BUFF 7
#define WRDEV 8
#define RDDEV 9
#define CPU 10
#define OMP 11

struct pfac_edge;

typedef struct pfac_text {
  int size;
  char *text;
} PFAC_TEXT_t;

typedef struct pfac_sint {
  int size;
  uint16_t *stream;
} PFAC_SINT_t;

typedef struct pfac_node {
  uint16_t id;
  short int final;
  struct pfac_edge *outgoing;
  uint16_t outgoing_degree;
} PFAC_NODE_t;

typedef struct pfac_edge {
  char alpha;        /* Edge alpha */
  PFAC_NODE_t *next; /* Target of the edge */
} PFAC_EDGE_t;

typedef struct {
  PFAC_NODE_t *root;
  PFAC_NODE_t **all_nodes;

  unsigned int all_nodes_num;   /* Number of all nodes in the automaton */
  unsigned long total_patterns; /* Total patterns in the automaton */
} PFAC_automaton_t;

uint16_t *pfac_automaton_search(PFAC_SINT_t *pfac_array_automaton,
                                PFAC_TEXT_t *input_stream, int root_id);
PFAC_automaton_t *pfac_build_automaton(char (*patterns)[MAX_PAT_LEN + 1],
                                       int num_patterns);
PFAC_SINT_t *pfac_convert_automaton(PFAC_automaton_t *pfac_automaton);
void print_measures();
char *get_pntr(size_t size);
void ocl_init();
#endif /* PFAC_H_ */
