#include "search.h"

extern int searchCpu(ReadFunction, MatchFunction);
extern int searchCpuEmulateGpu(ReadFunction, MatchFunction);
extern int searchGpu(ReadFunction, MatchFunction, PFAC_SINT_t *, int);

int search(ReadFunction read, MatchFunction onMatch,PFAC_SINT_t *array_automaton, int root_id) {
  if (SEARCH_WITH_GPU || HETEROGENEOUS_DESIGN) {
    return searchGpu(read, onMatch, array_automaton, root_id);
  }
  return searchCpu(read, onMatch);
}
