#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif

#include "math.h"
#include "stdio.h"
#include "stdlib.h"

#include "memory.h"
#include "search.h"
#include "shared-internal.h"
#include "timer.h"
#include "pfac.h"

cl_event resultEvent;
cl_event resultEvent2;

cl_event inputEvent;
cl_int clErr;

int cl_root_id;

#define CL_CHECK(_expr)                                                        \
  do {                                                                         \
    cl_int _err = _expr;                                                       \
    if (_err == CL_SUCCESS)                                                    \
      break;                                                                   \
    fprintf(stderr, "OpenCL Error: '%s' returned %d!\n", #_expr, (int)_err);   \
    abort();                                                                   \
  } while (0)

int should_measure = 0;

extern int exactMatchingUponFiltering(uint8_t *input, uint8_t *result,
                                      int length, DFC_PATTERNS *patterns,
                                      MatchFunction);
int getThreadCountForBytes(int size) {
  return ceil(size / (float)(THREAD_GRANULARITY));
}

void setKernelArgsNormalDesign(cl_kernel kernel, DfcOpenClBuffers *mem,
                               int readCount) {
  clSetKernelArg(kernel, 0, sizeof(int), &readCount);
  clSetKernelArg(kernel, 1, sizeof(cl_mem), &mem->input);

  clSetKernelArg(kernel, 2, sizeof(cl_mem), &mem->patterns);

  clSetKernelArg(kernel, 3, sizeof(cl_mem), &mem->dfSmall);
  clSetKernelArg(kernel, 4, sizeof(cl_mem), &mem->dfLarge);
  clSetKernelArg(kernel, 5, sizeof(cl_mem), &mem->dfLargeHash);

  clSetKernelArg(kernel, 6, sizeof(cl_mem), &mem->ctSmallEntries);
  clSetKernelArg(kernel, 7, sizeof(cl_mem), &mem->ctSmallPids);

  clSetKernelArg(kernel, 8, sizeof(cl_mem), &mem->ctLargeBuckets);
  clSetKernelArg(kernel, 9, sizeof(cl_mem), &mem->ctLargeEntries);
  clSetKernelArg(kernel, 10, sizeof(cl_mem), &mem->ctLargePids);

  clSetKernelArg(kernel, 11, sizeof(cl_mem), &mem->result);
  clSetKernelArg(kernel, 12, sizeof(cl_mem), &mem->intermResult);
  clSetKernelArg(kernel, 13, sizeof(cl_mem), &mem->intermResultLarge);
  clSetKernelArg(kernel, 14, sizeof(cl_mem), &mem->intermFlags);
}

void setKernelArgsHetDesign(cl_kernel kernel, DfcOpenClBuffers *mem,
                            int readCount) {
  clSetKernelArg(kernel, 0, sizeof(int), &readCount);
  clSetKernelArg(kernel, 1, sizeof(cl_mem), &mem->input);

  clSetKernelArg(kernel, 2, sizeof(cl_mem), &mem->dfSmall);
  clSetKernelArg(kernel, 3, sizeof(cl_mem), &mem->dfLarge);
  clSetKernelArg(kernel, 4, sizeof(cl_mem), &mem->dfLargeHash);

  clSetKernelArg(kernel, 5, sizeof(cl_mem), &mem->result);
  clSetKernelArg(kernel, 6, sizeof(cl_mem), &mem->cl_automaton_buff);
  clSetKernelArg(kernel, 7, sizeof(int), &cl_root_id );


}

void setKernelArgs(cl_kernel kernel, DfcOpenClBuffers *mem, int readCount) {
  if (HETEROGENEOUS_DESIGN) {
    setKernelArgsHetDesign(kernel, mem, readCount);
  } else {
    setKernelArgsNormalDesign(kernel, mem, readCount);
  }
}

size_t getGlobalGroupSize(size_t localGroupSize, int inputLength) {
  const float threadCount = getThreadCountForBytes(inputLength);

  const size_t globalGroupSize =
      ceil(threadCount / localGroupSize) * localGroupSize;

  return globalGroupSize;
}

void startKernelForQueue(cl_kernel kernel, cl_command_queue queue,
                         int inputLength) {
  const size_t localGroupSize = WORK_GROUP_SIZE;
  const size_t globalGroupSize =
      getGlobalGroupSize(localGroupSize, inputLength);

  //double timerStep = readTimerMs(TIMER_EXECUTE_KERNEL);
  startTimer(TIMER_EXECUTE_KERNEL);
  //CS: stoped using a fixed work group size, let the driver decicde
  int status;
  if (DISABLE_WG){
    status = clEnqueueNDRangeKernel(queue, kernel, 1, NULL, &globalGroupSize,
                                      NULL, 0, NULL, NULL);
  }
  else{
    status = clEnqueueNDRangeKernel(queue, kernel, 1, NULL, &globalGroupSize,
                                     &localGroupSize, 0, NULL, NULL);
    //status = CL_SUCCESS;
  }
  if (BLOCKING_DEVICE_ACCESS) {
    clFinish(queue);  // only necessary for timing
  }
  stopTimer(TIMER_EXECUTE_KERNEL);
  //printf("Kernel time(ms): %lf\n", readTimerMs(TIMER_EXECUTE_KERNEL)-timerStep);
  if (status != CL_SUCCESS) {
    fprintf(stderr, "Could not start kernel: %d\n", status);
    exit(OPENCL_COULD_NOT_START_KERNEL);
  }

  #if VECTORIZE_KERNEL && SEARCH_WITH_GPU
  // and again for verification phase
  //timerStep = readTimerMs(TIMER_EXECUTE_KERNEL);
  startTimer(TIMER_EXECUTE_KERNEL);
  //CS: stoped using a fixed work group size, let the driver decicde
  if (DISABLE_WG){
    status = clEnqueueNDRangeKernel(queue, kernel, 1, NULL, &globalGroupSize,
                                      NULL, 0, NULL, NULL);
  }
  else{
    status = clEnqueueNDRangeKernel(queue, kernel, 1, NULL, &globalGroupSize,
                                     &localGroupSize, 0, NULL, NULL);
    //status = CL_SUCCESS;
  }
  if (BLOCKING_DEVICE_ACCESS) {
    clFinish(queue);  // only necessary for timing
  }
  stopTimer(TIMER_EXECUTE_KERNEL);
  //printf("Kernel time(ms): %lf\n", readTimerMs(TIMER_EXECUTE_KERNEL)-timerStep);
  if (status != CL_SUCCESS) {
    fprintf(stderr, "Could not start kernel: %d\n", status);
    exit(OPENCL_COULD_NOT_START_KERNEL);
  }
  #endif

}

int handleMatches(uint16_t *result, int inputLength, DFC_PATTERNS *patterns,
                  MatchFunction onMatch) {
  VerifyResult *pidCounts = (VerifyResult *)result;

  int matches = 0;
  for (int i = 0; i < getThreadCountForBytes(inputLength); ++i) {
    VerifyResult *res = &pidCounts[i];

    for (uint8_t j = 0; j < res->matchCount && j < MAX_MATCHES_PER_THREAD;
         ++j) {
      onMatch(&patterns->dfcMatchList[res->matches[j]]);
      ++matches;
    }

    if (res->matchCount > MAX_MATCHES_PER_THREAD) {
      printf(
          "%d patterns matched at position %d, but space was only allocated "
          "for %d patterns\n",
          res->matchCount, i, MAX_MATCHES_PER_THREAD);
    }
  }
  return matches;
}

int handleResultsFromGpu(uint8_t *input, uint16_t *result, int inputLength,
                         DFC_PATTERNS *patterns, MatchFunction onMatch) {
  int matches;
  if (HETEROGENEOUS_DESIGN) {
    startTimer(TIMER_EXECUTE_HETEROGENEOUS);
    matches = exactMatchingUponFiltering(input, (uint8_t *)result, inputLength, patterns,
                                         onMatch);
    stopTimer(TIMER_EXECUTE_HETEROGENEOUS);
  } else {
    startTimer(TIMER_PROCESS_MATCHES);
    matches = handleMatches(result, inputLength, patterns, onMatch);
    stopTimer(TIMER_PROCESS_MATCHES);
  }

  return matches;
}

void readResultWithoutMap(DfcOpenClBuffers *mem, cl_command_queue queue,
                          int readCount, uint16_t *output) {
  bool blocking = OVERLAPPING_EXECUTION ? 0 : CL_BLOCKING;

  startTimer(TIMER_READ_FROM_DEVICE);
  int status = clEnqueueReadBuffer(queue, mem->result, blocking, 0,
                                   sizeInBytesOfResultVector(readCount), output,
                                   0, NULL, &resultEvent);
  stopTimer(TIMER_READ_FROM_DEVICE);

  if (status != CL_SUCCESS) {
    free(output);
    fprintf(stderr, "Could not read result: %d\n", status);
    exit(OPENCL_COULD_NOT_READ_RESULTS);
  }
}

uint16_t *readResultWithMap(DfcOpenClBuffers *mem, cl_command_queue queue,
                           int readCount) {
  cl_int status;
  bool blocking = OVERLAPPING_EXECUTION ? 0 : CL_BLOCKING;

  startTimer(TIMER_READ_FROM_DEVICE);
  uint16_t *output = clEnqueueMapBuffer(
      queue, mem->result, blocking, CL_MAP_READ, 0,
      sizeInBytesOfResultVector(readCount)*2, 0, NULL, &resultEvent, &status);
  stopTimer(TIMER_READ_FROM_DEVICE);

  //CS
  clFinish(queue);
  if (status != CL_SUCCESS) {
    fprintf(stderr, "Could not read result: %d\n", status);
    exit(OPENCL_COULD_NOT_READ_RESULTS);
  }

  return output;
}

// make sure to clean output later
void readResult(DfcOpenClBuffers *mem, cl_command_queue queue, int readCount,
                uint16_t **output) {
  if (MAP_MEMORY) {
    *output = readResultWithMap(mem, queue, readCount);
  } else {
    if (*output == NULL) {
      *output = calloc(1, sizeInBytesOfResultVector(INPUT_READ_CHUNK_BYTES));
    }
    readResultWithoutMap(mem, queue, readCount, *output);
  }
}

void cleanResult(cl_mem result, cl_command_queue queue, uint16_t **output) {
  if (MAP_MEMORY) {
    unmapOpenClBuffer(queue, *output, result);
  } else {
    free(*output);
    *output = NULL;
  }
}

int readResultAndCountMatches(uint8_t *input, DfcOpenClBuffers *mem,
                              cl_command_queue queue, DFC_PATTERNS *patterns,
                              int readCount, MatchFunction onMatch) {
  uint16_t *output = NULL;
  readResult(mem, queue, readCount, &output);
  int matches =
      handleResultsFromGpu(input, output, readCount, patterns, onMatch);
  cleanResult(mem->result, queue, &output);

  return matches;
}

void swapReadEvents() {
  cl_event tmp = resultEvent;
  resultEvent = resultEvent2;
  resultEvent2 = tmp;
}

cl_event getPrevReadEvent() { return resultEvent2; }

void waitForReadEvent(cl_event e) {
  startTimer(TIMER_READ_FROM_DEVICE);
  clWaitForEvents(1, &e);
  stopTimer(TIMER_READ_FROM_DEVICE);
}

void waitForWriteEvent(cl_event e) {
  startTimer(TIMER_WRITE_TO_DEVICE);
  clWaitForEvents(1, &e);
  stopTimer(TIMER_WRITE_TO_DEVICE);
}

void printKernelInfo(){
  printf("***** Kernel Info *****\n");
  printf("SEARCH_WITH_GPU: %d\n", SEARCH_WITH_GPU);
  printf("HETEROGENEOUS_DESIGN: %d\n",HETEROGENEOUS_DESIGN);
  printf("OVERLAPPING_EXECUTION: %d\n",OVERLAPPING_EXECUTION);
  printf("BLOCKING_DEVICE_ACCESS: %d\n",BLOCKING_DEVICE_ACCESS);
  printf("MAP_MEMORY: %d\n",MAP_MEMORY);
  printf("USE_TEXTURE_MEMORY: %d\n", USE_TEXTURE_MEMORY);
  printf("USE_LOCAL_MEMORY: %d\n",USE_LOCAL_MEMORY);
  printf("\n");
  printf("THREAD_GRANULARITY: %d\n",THREAD_GRANULARITY);
  printf("INPUT_READ_CHUNK_BYTES: %d\n",INPUT_READ_CHUNK_BYTES);
  printf("Threads normally spawned: %d\n", getGlobalGroupSize(WORK_GROUP_SIZE, INPUT_READ_CHUNK_BYTES));
  printf("VECTORIZE_KERNEL: %d\n",VECTORIZE_KERNEL);
  printf("\n");
  printf("WORK_GROUP_SIZE: %d\n",WORK_GROUP_SIZE);
  printf("DISABLE_WG: %d\n", DISABLE_WG);
  printf("MAX_MATCHES: %d\n",MAX_MATCHES);
  printf("MAX_MATCHES_PER_THREAD: %d\n",MAX_MATCHES_PER_THREAD);
  printf("**********************\n");
}

void ocl_buffer(PFAC_SINT_t *pfac_array_automaton){

  DFC_OPENCL_BUFFERS.cl_automaton_buff = clCreateBuffer(
      DFC_OPENCL_ENVIRONMENT.context, CL_MEM_READ_ONLY | CL_MEM_ALLOC_HOST_PTR,
      sizeof(uint16_t) * pfac_array_automaton->size, NULL, &clErr);
  clFinish(DFC_OPENCL_ENVIRONMENT.queue);
  uint16_t *automaton_ptr = (uint16_t *)clEnqueueMapBuffer(
      DFC_OPENCL_ENVIRONMENT.queue,  DFC_OPENCL_BUFFERS.cl_automaton_buff, CL_TRUE, CL_MAP_WRITE, 0,
      sizeof(uint16_t) * pfac_array_automaton->size, 0, NULL, NULL, &clErr);
  CL_CHECK(clErr);
  memcpy(automaton_ptr, pfac_array_automaton->stream,
         sizeof(uint16_t) * pfac_array_automaton->size);
  clEnqueueUnmapMemObject(DFC_OPENCL_ENVIRONMENT.queue,  DFC_OPENCL_BUFFERS.cl_automaton_buff, automaton_ptr, 0,
                          NULL, NULL);

  clFinish(DFC_OPENCL_ENVIRONMENT.queue);

}

int performSearch(ReadFunction read, MatchFunction onMatch, PFAC_SINT_t *array_automaton, int root_id) {
  char *input = getInputPtr();

  ocl_buffer(array_automaton);
  cl_root_id = root_id;
  printKernelInfo();
  uint16_t *output = NULL;

  uint8_t *prev_output = NULL;
  char *prev_input = NULL;
  int prev_readCount = 0;

  int matches = 0;
  int readCount = 0;
  should_measure = 1;
  while (
      (readCount = read(INPUT_READ_CHUNK_BYTES, MAX_PATTERN_LENGTH, input))) {
    writeInputBufferToDevice(input, readCount);

    setKernelArgs(DFC_OPENCL_ENVIRONMENT.kernel, &DFC_OPENCL_BUFFERS,
                  readCount);

    clFinish(DFC_OPENCL_ENVIRONMENT.queue);
                
    startKernelForQueue(DFC_OPENCL_ENVIRONMENT.kernel,
                        DFC_OPENCL_ENVIRONMENT.queue, readCount);

    if (shouldUseOverlappingExecution()) {
      swapOpenClInputBuffers();
      char *new_input = getOwnershipOfInputBufferAsync(&inputEvent);

      if (prev_input && prev_output) {
        waitForReadEvent(getPrevReadEvent());
        matches += handleResultsFromGpu(
            (uint8_t *)prev_input, output, prev_readCount,
            DFC_HOST_MEMORY.dfcStructure->patterns, onMatch);
        cleanResult(DFC_OPENCL_BUFFERS.result2, DFC_OPENCL_ENVIRONMENT.queue,
                    &output);
      }

      readResult(&DFC_OPENCL_BUFFERS, DFC_OPENCL_ENVIRONMENT.queue, readCount,
                 &output);

      prev_output = (uint8_t *)output;
      prev_input = input;
      prev_readCount = readCount;

      swapOpenClResultBuffers();
      swapReadEvents();

      waitForWriteEvent(inputEvent);
      input = new_input;
    } else {
      matches += readResultAndCountMatches(
          (uint8_t *)input, &DFC_OPENCL_BUFFERS, DFC_OPENCL_ENVIRONMENT.queue,
          DFC_HOST_MEMORY.dfcStructure->patterns, readCount, onMatch);
      input = getOwnershipOfInputBuffer();
    }
  }

  // have to handle the last one too
  if (shouldUseOverlappingExecution()) {
    if (prev_input && prev_output) {
      matches +=
          handleResultsFromGpu((uint8_t *)prev_input, output, prev_readCount,
                               DFC_HOST_MEMORY.dfcStructure->patterns, onMatch);
      cleanResult(DFC_OPENCL_BUFFERS.result2, DFC_OPENCL_ENVIRONMENT.queue,
                  &output);
    }
  }

  leaveOwnershipOfInputPointer(DFC_OPENCL_BUFFERS.input, input);

  printf("CPU + GPU TIME: %f ",readTimerMs(TIMER_EXECUTE_KERNEL)+readTimerMs(TIMER_PROCESS_MATCHES));

  return matches;
}

int searchGpu(ReadFunction read, MatchFunction onMatch, PFAC_SINT_t *array_automaton, int root_id) {
  return performSearch(read, onMatch, array_automaton, root_id);
}
