#ifndef DFC_SEARCH_H
#define DFC_SEARCH_H

#include "dfc.h"

int search(ReadFunction read, MatchFunction onMatch, PFAC_SINT_t *array_automaton, int root_id);
#endif
