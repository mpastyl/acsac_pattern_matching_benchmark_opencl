/*
 * pfac.c
 *
 *  Created on: Feb 11, 2014
 *      Author: arishop
 */
#include "pfac.h"

/* Allocation step for automaton.all_nodes */
#define CL_CHECK(_expr)                                                        \
  do {                                                                         \
    cl_int _err = _expr;                                                       \
    if (_err == CL_SUCCESS)                                                    \
      break;                                                                   \
    fprintf(stderr, "OpenCL Error: '%s' returned %d!\n", #_expr, (int)_err);   \
    abort();                                                                   \
  } while (0)

int inter_node_id;

int patterns_added=0;


// time management
/**************************************************************/
struct timeval start[50];
struct timeval stop[50];
float timeRes[50] = {0};
FILE *fio;
int gpuResult = 0;
int cpuResult = 0;
char hostName[50];
char description[256] = "Parallell Pattern Matching using Aho-Corasick";

void start_measure_time(int seg) { gettimeofday(&start[seg], NULL); }

void stop_measure_time(int seg) {
  gettimeofday(&stop[seg], NULL);
  timeRes[seg] += 1000 * ((float)(float)(stop[seg].tv_sec - start[seg].tv_sec) +
                         1.0e-6 * (stop[seg].tv_usec - start[seg].tv_usec));
}

// ocl setup

// print results
/**************************************************************************/
void print_measures() {
  char buff[256];
  fio = fopen("log.txt", "a+");
  fseek(fio, 0, SEEK_END);
  int appendPos = ftell(fio);

  float total_GPU_time = timeRes[PLATFORM] + timeRes[DEVICE] +
                         timeRes[CONTEXT] + timeRes[CMDQ] + timeRes[PGM] +
                         timeRes[KERNEL] + timeRes[BUFF] + timeRes[WRDEV] +
                         timeRes[KERNEL_EXEC] + timeRes[RDDEV];
  float total_GPU_fair_time =
      timeRes[KERNEL_EXEC] + timeRes[WRDEV] + timeRes[RDDEV];
  //	float total_GPU_fair_time = timeRes[KERNEL_EXEC];

  struct tm *local;
  time_t t;
  t = time(NULL);
  local = localtime(&t);

  fprintf(fio, "****************************************************\n");
  fprintf(fio, "Created on: %s", asctime(local));
  fprintf(fio, "Host name: %s \n", hostName);
  fprintf(fio, "Description: %s \n\n", description);
  fprintf(fio, "Result GPU is: %i \n", gpuResult);
  fprintf(fio, "Result CPU is: %i \n", cpuResult);
  if (cpuResult != gpuResult) {
    fprintf(fio, "ERROR Results do not match!!!!\n");
    printf("ERROR Results do not match!!!!\n");
    exit(1);
  }

  fprintf(fio, "\n===========Performance Measurements=================\n");

  //fprintf(fio, "WORK_GROUP_SIZE: %i\n", WORK_GROUP_SIZE);
  fprintf(fio, "MAX_PATS_NUM: %i\n", MAX_PATS_NUM);
  fprintf(fio, "MAX_IN_LEN: %i\n", MAX_IN_LEN);
  fprintf(fio, "MAX_NODES: %i\n", MAX_NODES);
  fprintf(fio, "MAX_PAT_LEN: %i\n", MAX_PAT_LEN);
  fprintf(fio, "THREAD GRAN: %i\n\n", FACTOR);

  fprintf(fio,
          "Execution times: \n"
          "	PLATFORM = \t%10.2f msecs \n"
          "	DEVICE = \t%10.2f msecs \n"
          "	CONTEXT = \t%10.2f msecs \n"
          "	CMDQ = \t\t%10.2f msecs \n"
          "	PGM = \t\t%10.2f msecs \n"
          "	KERNEL = \t%10.2f msecs \n"
          "	BUFF = \t\t%10.2f msecs \n"
          "	WRDEV = \t%10.2f msecs \n"
          "	KERNEL_EXEC = \t%10.2f msecs \n"
          "	RDDEV = \t%10.2f msecs \n\n",
          timeRes[PLATFORM], timeRes[DEVICE], timeRes[CONTEXT], timeRes[CMDQ],
          timeRes[PGM], timeRes[KERNEL], timeRes[BUFF], timeRes[WRDEV],
          timeRes[KERNEL_EXEC], timeRes[RDDEV]);
  fprintf(fio,
          "GPU time: \t\t%10.2f msecs \n"
          "GPU exec time: \t\t%10.2f msecs \n"
          "CPU time: \t\t%10.2f msecs \n\n",
          total_GPU_time, total_GPU_fair_time,
          timeRes[CPU]);
  fprintf(fio, "Speed UP (AC): \t\t%10.2f \n",
          (float)timeRes[CPU] / (float)total_GPU_fair_time);
  fprintf(fio, "Speed UP (PFAC_omp): \t%10.2f \n\n",
          (float)timeRes[OMP] / (float)total_GPU_fair_time);
  printf("time %10.2f \n", timeRes[20]);

  fseek(fio, appendPos, SEEK_SET);
  while (fgets(buff, sizeof buff, fio))
    printf("%s", buff);
  fclose(fio);
}


/***********************************************************************/
PFAC_NODE_t *pfac_node_create(void) {
  PFAC_NODE_t *thiz;
  thiz = (PFAC_NODE_t *)malloc(sizeof(PFAC_NODE_t));
  memset(thiz, 0, sizeof(PFAC_NODE_t));
  thiz->outgoing = (PFAC_EDGE_t *)malloc(MAX_OUTGOING * sizeof(PFAC_EDGE_t));
  return thiz;
}

PFAC_automaton_t *pfac_automaton_init() {
  PFAC_automaton_t *thiz = (PFAC_automaton_t *)malloc(sizeof(PFAC_automaton_t));
  memset(thiz, 0, sizeof(PFAC_automaton_t));
  thiz->root = pfac_node_create();
  thiz->all_nodes_num++;
  thiz->all_nodes = (PFAC_NODE_t **)malloc(MAX_NODES * sizeof(PFAC_NODE_t *));
  return thiz;
}

PFAC_NODE_t *pfac_node_create_next(PFAC_NODE_t *thiz, char alpha) {
  PFAC_NODE_t *next;
  next = pfac_node_create();
  thiz->outgoing[thiz->outgoing_degree].alpha = alpha;
  thiz->outgoing[thiz->outgoing_degree++].next = next;
  return next;
}

PFAC_NODE_t *pfac_node_find_next(PFAC_NODE_t *thiz, char alpha) {
  for (int i = 0; i < thiz->outgoing_degree; i++) {
    if (thiz->outgoing[i].alpha == alpha)
      return (thiz->outgoing[i].next);
  }
  return NULL;
}

void set_inter_ids(PFAC_NODE_t *current) {
  if (!current->final)
    current->id = inter_node_id++;
  for (int i = 0; i < current->outgoing_degree; i++)
    set_inter_ids(current->outgoing[i].next);
}

PFAC_automaton_t *pfac_build_automaton(char (*patterns)[MAX_PAT_LEN + 1],
                                       int num_patterns) {
  PFAC_automaton_t *pfac_automaton = pfac_automaton_init();
  PFAC_NODE_t *current;
  PFAC_NODE_t *next;
  char alpha;

  for (int i = 0; i < num_patterns; i++) {
    int j = 0;
    patterns_added++;
    current = pfac_automaton->root;

    while (1) {
      alpha = patterns[i][j];
      j++;
      if (alpha == '\a') { //I had to use a char not found in the pattern 
                           //file to signify the end of the pattern
        current->id = i + 1;
        current->final = 1;
        break;
      }
      if ((next = pfac_node_find_next(current, alpha)))
        current = next;
      else {
        next = pfac_node_create_next(current, alpha);
        current = next;
        pfac_automaton->all_nodes[pfac_automaton->all_nodes_num++] =
            current; // add the new node to automaton
        if (pfac_automaton->all_nodes_num > MAX_NODES) {
          printf("maximum number of nodes %d reached!!! \n", MAX_NODES);
          return NULL;
        }
      }
    }
  }
  inter_node_id = num_patterns + 1;
  set_inter_ids(pfac_automaton->root);
  // printf("total number of nodes: %d \n", pfac_automaton->all_nodes_num);
  return pfac_automaton;
}

void depth_first_traverse(PFAC_NODE_t *current, PFAC_SINT_t *array) {
  for (int i = 0; i < current->outgoing_degree; i++) {
    array->stream[(current->id - 1) * MAX_ALPHA +
                  (int)current->outgoing[i].alpha] =
        current->outgoing[i].next->id;
    depth_first_traverse(current->outgoing[i].next, array);
  }
}

PFAC_SINT_t *pfac_convert_automaton(PFAC_automaton_t *pfac_automaton) {
  PFAC_SINT_t *pfac_array_automaton = malloc(sizeof(PFAC_SINT_t));
  //Increased the size of the array automaton, otherwise we would jump out of it
	pfac_array_automaton->size = (pfac_automaton->all_nodes_num + patterns_added +1) * MAX_ALPHA;

  pfac_array_automaton->stream =
      malloc(pfac_array_automaton->size * sizeof(uint16_t));

  memset(pfac_array_automaton->stream, 0,
         pfac_array_automaton->size * sizeof(uint16_t));
  depth_first_traverse(pfac_automaton->root, pfac_array_automaton);
  return pfac_array_automaton;
}
