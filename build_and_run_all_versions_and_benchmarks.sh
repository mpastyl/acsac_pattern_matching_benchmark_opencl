#!/bin/bash
build_cur_dir="rm -rf build; mkdir build; cd build; cmake .. -DCMAKE_BUILD_TYPE=Release"


echo " ** Build all CPU VERSIONS **"

cd dfc-opencl
sed -i -e 's/DFC_SEARCH_WITH_GPU 1/DFC_SEARCH_WITH_GPU 0/g' CMakeLists.txt 
eval $build_cur_dir

cd ../../dfc-benchmark-libraries
eval $build_cur_dir

echo " ** Run benchmarks for CPU VERSIONS **"

cd ../../plotting_and_logs
mkdir logs 2>/dev/null
rm -rf logs/*

echo " ** Running Aho-Corasick on CPU all datasets and patterns (This version takes much longer than the others, approx. 20 min) **"
./throughput_ac.sh

echo " ** Running DFC on CPU all datasets and patterns **"
./throughput_dfc_cpu.sh

cd ../


echo " ** Build all GPU VERSIONS **"


cd dfc-opencl
sed -i -e 's/DFC_SEARCH_WITH_GPU 0/DFC_SEARCH_WITH_GPU 1/g' CMakeLists.txt 
eval $build_cur_dir

cd ../../dfc-benchmark-libraries
eval $build_cur_dir

cd ../../Hybrid/dfc-opencl-combined
eval $build_cur_dir

cd ../../dfc-benchmark-libraries-combined
eval $build_cur_dir

cd ../../dfc-opencl-combined-large-filter
eval $build_cur_dir

cd ../../dfc-benchmark-libraries-combined-large-filter
eval $build_cur_dir

echo " ** Run benchmarks for GPU VERSIONS **"

cd ../../../plotting_and_logs

echo " ** Running DFC on GPU all datasets and patterns **"
./throughput_dfc.sh
echo " ** Running DFC on GPU (vectorized) all datasets and patterns **"
./throughput_dfc_vec.sh
echo " ** Running HYBRID on GPU all datasets and patterns **"
./throughput_combined.sh
echo " ** Running HYBRID on GPU (vectorized) all datasets and patterns **"
./throughput_combined_vec.sh


echo " ** Running HYBRID with varying filter size **"
./df_size_combined.sh
./df_size_filtering.sh

echo " ** Finished running experiments and collecting data **"
