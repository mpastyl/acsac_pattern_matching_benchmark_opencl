#! /usr/bin/env python3

import sys
import re
import os
import csv
import glob
import pandas as pd

options = {
    'setup': 'setup',
    'teardown': 'teardown',
    'Preprocessing': 'preprocessing',
    'Reading data from file': 'read_file',
    'write to device': 'write_device',
    'read from device': 'read_device',
    'kernel': 'kernel',
    'GPU version': 'cpu',
    'Heterogeneous': 'cpu_het',
    'Total': 'total',
    'energy': 'energy'
}

filename_config_keys = [
    'where',
    'map',
    'thread_granularity',
    'memory_type',
    'vector',
    'workgroup_size',
    'chunk_size'
]


def main():
    if len(sys.argv) != 3:
        print("Usage: {} input-directory output-file")
        sys.exit(0)

    input_directory = sys.argv[1]
    output_file = sys.argv[2]

    results = {}
    for filename in os.listdir(input_directory):
        config = get_config(filename)
        if not config:
            continue

        energy = get_energy_avg(config, input_directory)
        values = parse_file(os.path.join(input_directory, filename))
        values['energy'] = energy

        if config in results:
            results[config].append(values)
        else:
            results[config] = [values]

    merged = merge_result(results)

    with open(output_file, 'w', newline='') as csvfile:
        fieldnames = filename_config_keys
        fieldnames.extend(options.values())
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

        writer.writeheader()

        for [keys, values] in merged.items():
            writer.writerow({**dict(keys), **values})


def get_config(filename):
    filename_regex = r'^(?P<where>\w+)_(?P<map>\w+)_th(?P<thread_granularity>\d+)_(?P<memory_type>\w+)_(?P<vector>\w+)_wg(?P<workgroup_size>\d+)(_chunk(?P<chunk_size>\d+))?_it\d$'
    rgx = re.compile(filename_regex)
    matches = rgx.match(filename)

    if not matches:
        return None

    values = matches.groupdict()

    # was not included at the time of execution
    if values['chunk_size'] == None:
        values['chunk_size'] = 10

    return frozenset(values.items())


def get_energy_avg(config, input_dir):
    config = dict(config)
    filename_glob = '{}_{}_th{}_{}_{}_wg{}_chunk{}_it*_energy'.format(config['where'], config['map'], config['thread_granularity'], config['memory_type'], config['vector'], config['workgroup_size'], config['chunk_size'])


    dfs = []
    for f in glob.glob(os.path.join(input_dir, filename_glob)):
        # First 2 seconds are sleeping
        dfs.append(pd.read_csv(f, header=0, skipinitialspace=True)[190:-190])

    return pd.concat(dfs).sum().sum() / len(dfs)

def parse_file(filename):
    vals = {}

    with open(filename) as f:
        for line in f:
            for key in options.keys():
                if key in line:
                    vals[options[key]] = last_as_number(line)

    return vals


def last_as_number(line: str):
    return float(line.split()[-1])


def merge_result(results):
    merged = {}
    for [config, values] in results.items():
        base = {}
        for key in options.values():
            for val in values:
                base[key] = base.get(key, float()) + val[key]

        if len(values) != 5:
            print('something is fishy')

        for key in base.keys():
            base[key] = base[key] / len(values)

        merged[config] = base

    return merged


if __name__ == '__main__':
    main()
