#! /usr/bin/env python3

import sys
import os
import re
import pandas as pd
import pathlib

PATTERN_LENGTH = 64

DATA_FILES = ['darpa_2000_outside',
              'iscx_2012_Sat', 'iscx_2012_Wed', 'random-1G']

PATTERN_FILES = ['http_related_rules', 'emerging_all',
                 # non null must come before emerging_http, otherwise its values ends up under emerging_http
                 'non_null_emerging_http', 'emerging_http']


def main():
    argv = sys.argv
    if len(argv) != 5:
        print(
            ('Usage: {} alg rgx input-directory output-directory\n' +
             'rgx must capture the total count in the first capture group').format(argv[0]))
        return

    alg = argv[1]
    rgx = argv[2]
    input_dir = argv[3]
    output_dir = argv[4]

    exec_time = {}
    energy = {}
    for f in os.listdir(input_dir):
        path = os.path.join(input_dir, f)

        data_file = find_data_file(f)
        pattern_file = find_pattern_file(f)

        key = (pattern_file, data_file)

        if 'energy' in f:
            energy[key] = energy.get(key, []) + [get_energy_total(path)]
        else:
            exec_time[key] = exec_time.get(key, []) + [get_total(rgx, path)]

    if exec_time.keys() != energy.keys():
        print('Unbalanced energy and exec measurements')
        exit(1)

    for key in exec_time.keys():
        pat, dat = key

        dir_path = os.path.join(output_dir, pat, dat)
        pathlib.Path(dir_path).mkdir(parents=True, exist_ok=True)

        with open(dir_path + '/' + alg + '.csv', 'w') as f:
            to_write = 'exec_time,energy\n'
            for ex, en in zip(exec_time[key], energy[key]):
                to_write += str(ex) + ',' + str(en) + '\n'

            f.write(to_write)


def find_data_file(f):
    for data_file in DATA_FILES:
        if data_file in f:
            return data_file

    return ''


def find_pattern_file(f):
    for pattern_file in PATTERN_FILES:
        name = pattern_file + '_' + str(PATTERN_LENGTH)
        if name in f:
            return name

    return ''


def get_total(rgx_str, filename):
    rgx = re.compile(rgx_str)

    with open(filename) as f:
        for line in f:
            matches = rgx.match(line)
            if matches:
                return float(matches.group(1))

    return 0


def get_energy_total(f):
    # skip first 1.9 s
    df = pd.read_csv(f, header=0, skipinitialspace=True)[190:]
    # sum columns and then sum rest
    return df.sum().sum()


def get_avg(d):
    res = {}
    for key, values in d.items():
        res[key] = sum(values) / len(values)

    return res


if __name__ == '__main__':
    main()
