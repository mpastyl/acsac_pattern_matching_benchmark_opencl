#! /bin/bash

make -C build -j8 benchmark-dfc-opencl > /dev/null

if [ $? -eq 0 ]
then
  cd ../dfc-opencl/build
  ../../dfc-benchmark-libraries/build/benchmarks/dfc-opencl/benchmark-dfc-opencl "$@"
  cd ../../dfc-benchmark-libraries
fi
