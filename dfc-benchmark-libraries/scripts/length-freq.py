#! /usr/bin/env python3

import sys
import matplotlib.pyplot as plt

def main():
    if len(sys.argv) != 3:
        executable_name = sys.argv[0]
        print(f"Usage: {executable_name} CUTOFF FILE")
        sys.exit(1)

    cutoff = int(sys.argv[1])
    filename = sys.argv[2]

    patterns = []
    with open(filename) as f:
        for pattern in f:
            patterns.append(pattern.rstrip())

    lengths = [len(x) for x in patterns]
    plt.hist(lengths, bins=[x for x in range(min(lengths), max(lengths))])
    plt.ylabel('Pattern count')
    plt.xlabel('Pattern length')
    plt.axvline(x=cutoff, color='red')

    plt.savefig('figure.pdf', bbox_inches='tight')

if __name__ == '__main__':
    main()