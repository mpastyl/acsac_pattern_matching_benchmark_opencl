#! /bin/bash

make -C build -j8 benchmark-aho-corasick > /dev/null

if [ $? -eq 0 ]
then
  pattern_file="$(realpath $1)"
  data_file="$(realpath $2)"

  ./build/benchmarks/aho-corasick/benchmark-aho-corasick "$pattern_file" "$data_file"
fi
