#! /bin/bash

make -C build -j8 benchmark-aho-corasick-snort > /dev/null

if [ $? -eq 0 ]
then
  ./build/benchmarks/aho-corasick-snort/benchmark-aho-corasick-snort "$@"
fi
