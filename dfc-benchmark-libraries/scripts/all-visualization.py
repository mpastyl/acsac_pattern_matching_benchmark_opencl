#! /usr/bin/env python3

import sys
import os
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import pathlib

FANCY_ALG_NAME = {
    'ac': 'Aho-Corasick',
    'dfc-cpu': 'DFC (CPU)',
    'dfc-gpu': 'DFC (GPU)',
    'dfc-het': 'DFC (HET)',
    'dfc-gpu-overlapping': 'DFC (GPU, Overlapping)',
    'dfc-het-overlapping': 'DFC (HET, Overlapping)'
}
FILE_SIZES = {
    'darpa_2000_outside': 168927947,
    'iscx_2012_Sat': 1075618151,
    'iscx_2012_Wed': 846480924,
    'random-1G': 1073741824
}

TRAFFIC_ORDER = [
    'darpa_2000_outside',
    'iscx_2012_Wed',
    'iscx_2012_Sat',
    'random-1G'
]

FANCY_TRAFFIC_NAME = [
    'Darpa', 'ISCX day 2', 'ISCX day 6', 'Random'
]


def main():
    argv = sys.argv
    if len(argv) <= 3:
        print('Usage: {} input-directory output-directory alg [alg, alg...]')
        return

    input_dir = argv[1]
    output_dir = argv[2]
    algs = argv[3:]

    dfs_thr = {}
    dfs_ene = {}
    for pat in os.listdir(input_dir):
        pat_dir = os.path.join(input_dir, pat)
        for dat in os.listdir(pat_dir):
            dat_dir = os.path.join(pat_dir, dat)

            key = (pat, dat)
            for alg_file in os.listdir(dat_dir):
                alg_path = os.path.join(dat_dir, alg_file)

                alg_from_file = os.path.splitext(alg_file)[0]
                if alg_from_file not in algs:
                    continue

                alg_name = FANCY_ALG_NAME[alg_from_file]
                df = pd.read_csv(alg_path, header=0,
                                 skipinitialspace=True)

                exec_time = df['exec_time'].rename(alg_name)

                throughput = (FILE_SIZES[dat] / 1E6) / \
                    (exec_time / 1E3)  # B/ms => MB/s

                dfs_thr[key] = pd.concat(
                    [dfs_thr.get(key, None), throughput], axis=1)
                dfs_ene[key] = pd.concat(
                    [dfs_ene.get(key, None), (df['energy'] / 1E3).rename(alg_name)], axis=1)

    pathlib.Path(output_dir).mkdir(parents=True, exist_ok=True)

    plot('Throughput (MB/s)', dfs_thr, output_dir, 'throughput_')
    plot('Energy (kJ)', dfs_ene, output_dir, 'energy_')


def avg(d):
    return {key: df.mean(axis=0)[sorted(df.columns)].to_frame(key[1])
            for key, df in d.items()}


def std(d):
    return {key: df.std()[sorted(df.columns)].to_frame(key[1])
            for key, df in d.items()}


def to_nice_shape(d):
    to_plot = {}
    for (pat, dat), df in d.items():
        df_res = to_plot.get(pat, pd.DataFrame())
        df_res = pd.concat([df_res, df], axis=1)
        to_plot[pat] = df_res

    for pat in to_plot.keys():
        to_plot[pat] = to_plot[pat][TRAFFIC_ORDER]
        to_plot[pat].columns = FANCY_TRAFFIC_NAME
        to_plot[pat] = to_plot[pat].transpose()

    return to_plot


def plot(y_label, d, output_dir, file_prefix):
    avg_to_plot = to_nice_shape(avg(d))
    std_to_plot = to_nice_shape(std(d))

    for pat in avg_to_plot.keys():
        ax = avg_to_plot[pat].plot.bar(
            yerr=std_to_plot[pat], width=0.8, figsize=(10, 5))

        ratios = [''] * len(ax.patches)
        for start in range(len(FANCY_TRAFFIC_NAME)):
            r = ax.patches[start].get_height()
            if r == 0.0:
                r = ax.patches[start + len(FANCY_TRAFFIC_NAME)].get_height()
            for i, p in enumerate(ax.patches[start::len(FANCY_TRAFFIC_NAME)]):
                height = p.get_height()

                if height != 0.0:
                    ratios[i * len(FANCY_TRAFFIC_NAME) +
                           start] = "{:.2f}".format(height / r)

        for r, p in zip(ratios, ax.patches):
            ax.annotate(r, (p.get_x() * 1.005, p.get_height() * 1.01))

        ax.set_title(pat)
        ax.set_ylabel(y_label)
        if 'http_related_rules' in pat and 'throughput' in file_prefix:
            ax.set_ylim(0, 115)

        if 'http_related_rules' in pat and 'energy' in file_prefix:
            ax.set_ylim(0, 12)

        if 'emerging_http' in pat and 'energy' in file_prefix:
            ax.set_ylim(0, 15)

        if 'emerging_all' in pat and 'energy' in file_prefix:
            ax.set_ylim(0, 38)

        ax.get_figure().savefig(os.path.join(
            output_dir, file_prefix + pat + '.pdf'), bbox_inches='tight')


if __name__ == '__main__':
    matplotlib.rcParams.update({'font.size': 14})
    main()
