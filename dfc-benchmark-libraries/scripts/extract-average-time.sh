#! /usr/bin/env bash

# Number must be the LAST in each line

lines=$(grep "$@")
numbers=$(awk 'NF{ print $NF }' <<< "$lines")
sum=$(awk '{sum += $0} END { printf("%.11G\n", sum)}' <<< $numbers)
line_count=$(wc -l <<< $numbers)
avg=$(bc <<< "scale=5; $sum / $line_count")

echo "Average: $avg"