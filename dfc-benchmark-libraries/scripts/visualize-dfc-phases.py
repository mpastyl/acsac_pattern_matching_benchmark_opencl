#! /usr/bin/env python3

import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import sys


def main():
    if len(sys.argv) < 3:
        print('Usage: {} y-lim FILE, [FILE, FILE...]'.format(sys.argv[0]))
        sys.exit(1)

    y_lim = float(sys.argv[1])
    filenames = sys.argv[2:]
    dfs = []
    for name in filenames:
        with open(name) as f:
            df = pd.read_csv(f, header=0, skipinitialspace=True)
            dfs.append(df)

    # remove the first and last second (because of sleep)
    a15w = pd.concat([df['a15-watt']
                      for df in dfs], axis=1).mean(axis=1).rename('A15')
    a7w = pd.concat([df['a17-watt']
                      for df in dfs], axis=1).mean(axis=1).rename('A7')
    gpuw = pd.concat([df['gpu-watt']
                      for df in dfs], axis=1).mean(axis=1).rename('GPU')
    memw = pd.concat([df['mem-watt']
                      for df in dfs], axis=1).mean(axis=1).rename('MEM')
    sumw = pd.concat([a15w, a7w, gpuw, memw], axis=1).sum(axis=1).rename('SUM')

    df = pd.concat([a15w, a7w, gpuw, memw, sumw], axis=1)[190:-100]


    ax = df.plot()
    ax.set_ylim(-0.1, y_lim)
    ax.set_xlabel('Time (10ms)')
    ax.set_ylabel('Watt')

    ax.get_figure().savefig('figure.pdf', bbox_inches='tight')
    
    #plt.show()

if __name__ == '__main__':
    #matplotlib.rcParams.update({'font.size': 22})
    main()
