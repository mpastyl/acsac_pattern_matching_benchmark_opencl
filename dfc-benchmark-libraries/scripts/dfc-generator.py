#!/usr/bin/env python3

import subprocess
import sys
import os
import time
import signal

# (DFC_SEARCH_WITH_GPU 0)
# (DFC_HETEROGENEOUS_DESIGN 0)
#
# (DFC_MAP_MEMORY 0)
#
# (DFC_VECTORIZE_KERNEL 0)
# (DFC_USE_TEXTURE_MEMORY 0)
# (DFC_USE_LOCAL_MEMORY 0)
#
# (DFC_WORK_GROUP_SIZE 256)
#
# (DFC_THREAD_GRANULARITY 16)
#
# (DFC_INPUT_READ_CHUNK_BYTES 50000000)
# (DFC_BLOCKING_DEVICE_ACCESS 1)


class Config:
    def __init__(self, exec_mode='cpu', mmap=0, mem_mode='non', wgsize=0, thread_gran=0, chunk_size_mb=0, overlap=0):
        self.cpu = 0
        self.gpu = 0
        self.het = 0

        if exec_mode == 'cpu':
            self.cpu = 1
        elif exec_mode == 'gpu':
            self.gpu = 1
        elif exec_mode == 'het':
            self.het = 1

        self.mmap = mmap

        self.vec = 0
        self.text = 0
        self.local = 0
        if mem_mode == 'vec':
            self.vec = 1
        elif mem_mode == 'tex':
            self.text = 1
        elif mem_mode == 'loc':
            self.local = 1

        self.wgsize = wgsize
        self.thread_gran = thread_gran
        self.chunk_size_mb = chunk_size_mb
        self.overlap = overlap

    def update_config(self, cmake_path):
        self.update_value(cmake_path, 'DFC_SEARCH_WITH_GPU', self.gpu)
        self.update_value(cmake_path, 'DFC_HETEROGENEOUS_DESIGN', self.het)

        self.update_value(cmake_path, 'DFC_MAP_MEMORY', self.mmap)

        self.update_value(cmake_path, 'DFC_VECTORIZE_KERNEL', self.vec)
        self.update_value(cmake_path, 'DFC_USE_TEXTURE_MEMORY', self.text)
        self.update_value(cmake_path, 'DFC_USE_LOCAL_MEMORY', self.local)

        self.update_value(cmake_path, 'DFC_WORK_GROUP_SIZE', self.wgsize)
        self.update_value(
            cmake_path, 'DFC_THREAD_GRANULARITY', self.thread_gran)
        self.update_value(
            cmake_path, 'DFC_INPUT_READ_CHUNK_BYTES', int(self.chunk_size_mb * 1E6))
        self.update_value(
            cmake_path, 'DFC_OVERLAPPING_EXECUTION', self.overlap)


    def update_value(self, cmake_path, key, val):
        #print(self.sed_args(cmake_path, key, val))
        if subprocess.call(self.sed_args(cmake_path, key, val)):
            print(self.sed_args(cmake_path, key, val))
            exit(1)

    def sed_args(self, cmake_path, key, val):
        args = ['sed', '-i',
                's/set({} .*/set({} {})/'.format(key, key, val), cmake_path]
        return args

    def name(self):
        out = []
        if self.gpu:
            out.append('g')
        elif self.het:
            out.append('h')
        else:
            out.append('c')

        if self.mmap:
            out.append('map')
        else:
            out.append('xxx')

        out.append('th' + str(self.thread_gran))

        if self.text:
            out.append('tex')
        elif self.local:
            out.append('loc')
        else:
            out.append('nor')

        if self.vec:
            out.append('vec')
        else:
            out.append('xxx')

        out.append('wg' + str(self.wgsize))
        out.append('chunk' + str(self.chunk_size_mb))

        return '_'.join(out)


def main():
    argv = sys.argv
    if len(argv) != 6:
        print(
            'Usage: {} cmake-config-file pattern-file data-file out-directory iterations'.format(argv[0]))
        sys.exit(1)

    cmake_path = argv[1]
    pattern_file = os.path.realpath(argv[2])
    data_file = os.path.realpath(argv[3])
    out_directory = argv[4]
    iterations = int(argv[5])

    if not os.path.exists(out_directory):
        os.makedirs(out_directory)


    perms = generate_permutations()
    for perm in perms:
        perm.update_config(cmake_path)

        subprocess.run(['make', '-C' 'build', '-j8', 'benchmark-dfc-opencl'])

        for it in range(iterations):
            print('Executing iteration: {}'.format(it))
            execute_script(perm.name(), pattern_file,
                           data_file, out_directory, it)


# Once with CPU
# GPU / Het
# with / without MAP
# with / without texture OR local OR vectorize
# workgroup: 64, 128, 256
# granularity: 1, 8, 16
def generate_permutations():
    perms = []

    perms.append(Config(exec_mode='cpu', mmap=1,
                        mem_mode='nor', wgsize=128, thread_gran=40,
                        chunk_size_mb=25, overlap=0))

    return perms


def execute_script(perm_name, pattern_file, data_file, out_dir, it):
    args = ['../../dfc-benchmark-libraries/build/benchmarks/dfc-opencl/benchmark-dfc-opencl', pattern_file, data_file]
    filename = out_dir + '/' + perm_name + '_it' + str(it)
    energy_filename = filename + '_energy'

    pid = None
    try:
        with open(energy_filename, 'wb') as energy_file:
            pid = subprocess.Popen("energy-monitor", stdout=energy_file).pid

            time.sleep(2)

            output = subprocess.check_output(args, cwd='../dfc-opencl/build')

            time.sleep(2)

            os.kill(pid, signal.SIGTERM)

            with open(out_dir + '/' + perm_name + '_it' + str(it), 'wb') as out_file:
                out_file.write(output)

    except subprocess.CalledProcessError as e:
        os.kill(pid, signal.SIGTERM)
        print('{} failed with value {}!'.format(
            str(args), e.returncode))


if __name__ == '__main__':
    main()
