#! /usr/bin/env python3

import sys
import time
import os
import subprocess
import signal

PATTERN_LENGTH = 64

DATA_FILE_PATH = 'traffic/'
DATA_FILES = ['darpa_2000_outside',
              'iscx_2012_Wed', 'random-1G', 'iscx_2012_Sat']

PATTERN_FILE_PATH = 'patterns/'
PATTERN_FILES = ['http_related_rules',
                 'emerging_http', 'emerging_all']


class ParsedArgv:
    def __init__(self, argv):
        self.alg = argv[1]
        self.script = argv[2]
        self.input_directory = os.path.realpath(argv[3]) + '/'
        self.output_directory = os.path.realpath(argv[4]) + '/'
        self.iterations = int(argv[5])
        self.energy = len(argv) == 7


class BenchmarkPermutation:
    def __init__(self, algorithm_name, input_directory, output_directory, data, pattern):
        self.data = concat_file(input_directory, DATA_FILE_PATH, data)

        self.pattern = concat_file(
            input_directory, PATTERN_FILE_PATH, pattern_name(pattern, PATTERN_LENGTH))
        self.output = concat_file(
            output_directory, '', output_name(algorithm_name, data, pattern_name(pattern, PATTERN_LENGTH)))


def concat_file(working_directory, directory, filename):
    return os.path.join(os.path.realpath(working_directory), directory, filename)


def pattern_name(pattern, pattern_length):
    return pattern + '_' + str(pattern_length)


def output_name(alg, data, pattern):
    return alg + '_' + pattern + '_' + data


def main():
    if not valid_argv(sys.argv):
        print_usage(sys.argv)
        return 1

    arguments = ParsedArgv(sys.argv)

    if not os.path.exists(arguments.output_directory):
        os.makedirs(arguments.output_directory)

    permutations = create_permutations(arguments)

    validate_permutations(permutations)

    for it in range(arguments.iterations):
        print('Executing iteration: {}'.format(it))
        execute_permutations(arguments.alg, arguments.script,
                             permutations, it, arguments.energy)


def valid_argv(argv):
    return len(argv) == 6 or len(argv) == 7


def print_usage(argv):
    executable_name = argv[0]
    print(
        'Usage: {} alg script input-directory output-directory iterations [TRUE|FALSE (energy-measurements)]'.format(executable_name))


def create_permutations(arguments):
    perms = []

    for pattern in PATTERN_FILES:
        for data in DATA_FILES:
            perms.append(BenchmarkPermutation(algorithm_name=arguments.alg,
                                              input_directory=arguments.input_directory, output_directory=arguments.output_directory, data=data, pattern=pattern))
    return perms


def validate_permutations(permutations):
    for perm in permutations:
        validate_input_files(perm)


def validate_input_files(permutation):
    if not file_exists(permutation.data):
        print(permutation.data + ' does not exist')
        sys.exit(1)
    if not file_exists(permutation.pattern):
        print(permutation.pattern + ' does not exist')
        sys.exit(2)


def file_exists(f):

    return os.path.isfile(f)


def execute_permutations(alg, script, permutations, it, energy):
    for perm in permutations:
        pattern = perm.pattern
        data = perm.data

        print('Now running: {} against {}'.format(
            os.path.basename(pattern), os.path.basename(data)))

        pid = None
        try:
            args = executable_arguments(script, perm)

            if alg == 'ac' or ('dfc' in alg and energy):
                print('Letting the benchmark itself handle energy measurements')
                args.append(perm.output + '_it' + str(it) + '_energy')

                output = subprocess.check_output(args)

                with open(perm.output + '_it' + str(it), 'wb') as out_file:
                    out_file.write(output)
            else:
                with open(perm.output + '_it' + str(it) + '_energy', 'wb') as energy_file:

                    pid = subprocess.Popen(
                        "energy-monitor", stdout=energy_file).pid

                    time.sleep(2)

                    output = subprocess.check_output(args)

                    time.sleep(2)

                    os.kill(pid, signal.SIGTERM)

                    with open(perm.output + '_it' + str(it), 'wb') as out_file:
                        out_file.write(output)

        except subprocess.CalledProcessError as e:
            if pid:
                os.kill(pid, signal.SIGTERM)
            print('{} failed with value {}!'.format(
                str(executable_arguments(script, perm)), e.returncode))


def executable_arguments(script, perm):
    args = [script, perm.pattern]
    args.append(perm.data)
    return args


if __name__ == '__main__':
    main()
