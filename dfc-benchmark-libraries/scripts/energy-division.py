#! /usr/bin/env python3

import pandas as pd
import sys


def main():
    if len(sys.argv) < 2:
        print('Usage: {} FILE, [FILE, FILE...]'.format(sys.argv[0]))
        sys.exit(1)

    filenames = sys.argv[1:]
    dfs = []
    for name in filenames:
        with open(name) as f:
            df = pd.read_csv(f, header=0, skipinitialspace=True)[190:]
            dfs.append(df)

    df = pd.concat(dfs).sum()
    total = df.sum()
    percents = ((df / total) * 100).round(decimals=0)
    print(percents)

if __name__ == '__main__':
    main()
