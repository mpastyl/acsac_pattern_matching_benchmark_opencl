#! /bin/bash

if [ $# -ne 2 ]
then
  echo "Usage: $0 PATTERN-FILE DATA-FILE"
  exit 0
fi

if [ ! -f $1 ] || [ ! -f $2 ]
then
  echo "Files must exist"
  exit 1
fi

pattern_file="$(realpath $1)"
data_file="$(realpath $2)"

cd build

rm __*
rm filtered_*.out

if [ ! -e "./ffbf-page" ]
then
  echo "Mounting huge page file"
  sudo tee /proc/sys/vm/nr_hugepages <<< 100 > /dev/null
  mkdir ./ffbf-page
  sudo mount -t hugetlbfs nodev ./ffbf-page
  sudo chmod ugo+rw ./ffbf-page
fi

if [ ! -f "../../ffbf/src/rabin-karp/ffbf" ]
then
  echo "Making ffbf"
  cd ../../ffbf
  autoreconf -fi
  make -j8

  cd ../../dfc-benchmark-libraries/build
fi

#echo "start "$pattern_file" filtered_patterns.out < "$data_file" > filtered_corpus.out"
../../ffbf/src/rabin-karp/ffbf "$pattern_file" filtered_patterns.out < "$data_file" > filtered_corpus.out
grep -c -Ff filtered_patterns.out filtered_corpus.out