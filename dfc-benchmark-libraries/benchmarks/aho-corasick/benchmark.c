#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ahocorasick.h"
#include "argument-parser.h"
#include "parser.h"
#include "benchmark-timer.h"

void add_pattern(AC_automaton_t *acap, unsigned char* pattern, int length, int case_insensitive, int pattern_id) {
  (void)(case_insensitive);

  AC_PATTERN_t tmp_patt;
  tmp_patt.length = length;
  tmp_patt.astring = malloc(length);
  memcpy(tmp_patt.astring, (char*)pattern, length);

  tmp_patt.rep.number = pattern_id;

  ac_automaton_add(acap, &tmp_patt);
}

long match_count;

int match_handler(AC_MATCH_t *m, void *param) {
  (void)(param);
  match_count += m->match_num;

  return 0;
}

int main(int argc, char** argv) {
  validate_arguments(argc, argv);

  FILE *i1_file;
  i1_file = fopen(get_data_file(argv), "rb");
  fseek(i1_file, 0, SEEK_END);
  size_t filelen1 = ftell(i1_file);

  char *input_text = malloc(sizeof(char) * (filelen1));

  rewind(i1_file);

  size_t readlen = fread(input_text, 1, filelen1, i1_file);
  if (readlen < filelen1)
    input_text[readlen] = '\0';
  else
    input_text[filelen1] = '\0';
  fclose(i1_file);

  AC_automaton_t *acap;
  AC_TEXT_t tmp_text;
  acap = ac_automaton_init(match_handler);

  TIME("add patterns", parse_pattern_file(get_pattern_file(argv), acap, (AddPattern)add_pattern));

  // Finalize automaton.
  TIME("finalizing preprocessing", ac_automaton_finalize(acap));

  // Set input text
  tmp_text.astring = input_text;
  tmp_text.length = filelen1;

  TIME("searching", ac_automaton_search(acap, &tmp_text, 0));
  printf("\n* Total match count: %ld\n", match_count);

  ac_automaton_reset(acap);

  ac_automaton_release(acap);

  free(input_text);
}
