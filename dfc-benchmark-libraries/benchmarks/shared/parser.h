#ifndef DFC_BENCHMARK_PARSER_H
#define DFC_BENCHMARK_PARSER_H

#ifdef __cplusplus
extern "C" {
#endif

typedef void (*AddPattern)(void *, unsigned char *, int, int,
                           unsigned int);

void parse_pattern_file(const char *file_name, void *pattern_init,
                        AddPattern add_pattern);

#ifdef __cplusplus
}
#endif

#endif
