mkdir final_plots 2>/dev/null
cd plotting_and_logs
python plot_throughput.py
python plot_throughput_all_data_patterns.py
python plot_df_size.py
cd ../

echo "Done. All plots are created in the final_plots folder"
