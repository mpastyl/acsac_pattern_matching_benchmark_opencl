# Co-Evaluation of Pattern Matching Algorithms on IoT Devices with Embedded GPUs

Artifact submission for ACSAC 2019.


## Requirements
- CMake (3.1 or greater)
- OpenCL (1.2 or greater)
- C compiler
- C++ compiler (C++11 or greater)
- git

## How to Use

Run
```sh
./build_and_run_all_versions_and_benchmarks.sh
```
to build the code and the benchmarks and run all experiments. This might take roughly 30 minutes.


Run
```sh
./get_all_plots.sh
```
to recreate the corresponding figures from the paper. The figures will be stored in the `final_plots` directory.  


## Data

Data used in the paper are available at https://chalmersuniversity.box.com/s/t2dp947trx2i1h5ld627yepuoh87dnep


## Code structure

`dfc-opecl` contains the main code for the GPU version of DFC used in the paper.

`dfc-benchmark-libraries` contains the main benchmark for each version, as well as the (CPU) implementation of Aho-Corasick used in Snort.

The `Hybrid` directory contains much of the same code, but the DFC version has been altered to implement the HYBRID version described in the paper, which is a mixture of DFC and PFAC.

`plotting_and_logs` contains script that run and plot the result of the experiments.

## Acknowledgements

This repository is a bundle of implementations and benchmarks developed by Simon Kindstrom [sindstrom/dfc](https://github.com/skindstrom/dfc), modified and extended by Charalampos Stylianopoulos.

